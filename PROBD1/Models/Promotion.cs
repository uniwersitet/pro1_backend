﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Promotion
    {
        public Promotion()
        {
            Koszyk = new HashSet<Koszyk>();
            Pizza.Promotion.Add(this);
        }

        public int Id { get; set; }
        public decimal DueAmount { get; set; }
        public int PizzaId { get; set; }

        public Pizza Pizza { get; set; }
        public ICollection<Koszyk> Koszyk { get; set; }
    }
}
