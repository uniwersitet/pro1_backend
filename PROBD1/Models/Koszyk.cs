﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Koszyk
    {
        public Koszyk()
        {
            Client.Koszyk.Add(this);
            Pizza.Koszyk.Add(this);
            Promotion.Koszyk.Add(this);
        }
        public int Id { get; set; }
        public int PizzaId { get; set; }
        public int ClientId { get; set; }
        public int PromotionId { get; set; }

        public Client Client { get; set; }
        public Pizza Pizza { get; set; }
        public Promotion Promotion { get; set; }
    }
}
