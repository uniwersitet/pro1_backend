﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Client
    {
        public Client()
        {
            Koszyk = new HashSet<Koszyk>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int AddressId { get; set; }

        public Address Address { get; set; }
        public ICollection<Koszyk> Koszyk { get; set; }
    }
}
