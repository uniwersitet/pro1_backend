﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Address
    {
        public Address()
        {
            Client = new HashSet<Client>();
        }

        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Zipcode { get; set; }
        public string HomeNr { get; set; }
        public int? AptNr { get; set; }

        public ICollection<Client> Client { get; set; }
    }
}
