﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class PizzaIngredient
    {
        public PizzaIngredient()
        {
            Pizza.PizzaIngredient.Add(this);
            Ingredient.PizzaIngredient.Add(this);

        }
        public int IngredientId { get; set; }
        public int PizzaId { get; set; }

        public Ingredient Ingredient { get; set; }
        public Pizza Pizza { get; set; }
    }
}
