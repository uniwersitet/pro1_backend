﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Pizza
    {
        public Pizza()
        {
            Koszyk = new HashSet<Koszyk>();
            PizzaIngredient = new HashSet<PizzaIngredient>();
            Promotion = new HashSet<Promotion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal DueAmount { get; set; }

        public ICollection<Koszyk> Koszyk { get; set; }
        public ICollection<PizzaIngredient> PizzaIngredient { get; set; }
        public ICollection<Promotion> Promotion { get; set; }
    }
}
