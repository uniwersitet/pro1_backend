﻿using System;
using System.Collections.Generic;

namespace PROBD1.Models
{
    public partial class Ingredient
    {
        public Ingredient()
        {
            PizzaIngredient = new HashSet<PizzaIngredient>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<PizzaIngredient> PizzaIngredient { get; set; }
    }
}
