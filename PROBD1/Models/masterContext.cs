﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PROBD1.Models
{
    public partial class masterContext : DbContext
    {
        public masterContext()
        {
        }

        public masterContext(DbContextOptions<masterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Ingredient> Ingredient { get; set; }
        public virtual DbSet<Koszyk> Koszyk { get; set; }
        public virtual DbSet<Pizza> Pizza { get; set; }
        public virtual DbSet<PizzaIngredient> PizzaIngredient { get; set; }
        public virtual DbSet<Promotion> Promotion { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               optionsBuilder.UseSqlServer("Data Source=LAPTOP-4VV83EOT\\SQLEXPRESS;Initial Catalog=master;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AptNr).HasColumnName("apt_nr");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(50);

                entity.Property(e => e.HomeNr)
                    .IsRequired()
                    .HasColumnName("home_nr")
                    .HasMaxLength(10);

                entity.Property(e => e.Street)
                    .HasColumnName("street")
                    .HasMaxLength(128);

                entity.Property(e => e.Zipcode)
                    .IsRequired()
                    .HasColumnName("zipcode")
                    .HasMaxLength(8);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("Client", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AddressId).HasColumnName("Address_id");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(25);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(32);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(32);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(14);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Client)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Client_Address");
            });

            modelBuilder.Entity<Ingredient>(entity =>
            {
                entity.ToTable("Ingredient", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Koszyk>(entity =>
            {
                entity.ToTable("Koszyk", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("Client_id");

                entity.Property(e => e.PizzaId).HasColumnName("Pizza_id");

                entity.Property(e => e.PromotionId).HasColumnName("Promotion_id");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Koszyk)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Koszyk_Client");

                entity.HasOne(d => d.Pizza)
                    .WithMany(p => p.Koszyk)
                    .HasForeignKey(d => d.PizzaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Koszyk_Pizza");

                entity.HasOne(d => d.Promotion)
                    .WithMany(p => p.Koszyk)
                    .HasForeignKey(d => d.PromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Koszyk_Promotion");
            });

            modelBuilder.Entity<Pizza>(entity =>
            {
                entity.ToTable("Pizza", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DueAmount)
                    .HasColumnName("due_amount")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<PizzaIngredient>(entity =>
            {
                entity.HasKey(e => new { e.IngredientId, e.PizzaId });

                entity.ToTable("Pizza_ingredient", "PROBD");

                entity.Property(e => e.IngredientId).HasColumnName("Ingredient_id");

                entity.Property(e => e.PizzaId).HasColumnName("Pizza_id");

                entity.HasOne(d => d.Ingredient)
                    .WithMany(p => p.PizzaIngredient)
                    .HasForeignKey(d => d.IngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Pizza_ingredient_Ingredient");

                entity.HasOne(d => d.Pizza)
                    .WithMany(p => p.PizzaIngredient)
                    .HasForeignKey(d => d.PizzaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Pizza_ingredient_Pizza");
            });

            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.ToTable("Promotion", "PROBD");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DueAmount)
                    .HasColumnName("due_amount")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.PizzaId).HasColumnName("Pizza_id");

                entity.HasOne(d => d.Pizza)
                    .WithMany(p => p.Promotion)
                    .HasForeignKey(d => d.PizzaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Promotion_Pizza");
            });
        }
    }
}
