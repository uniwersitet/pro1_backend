﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROBD1.Models;

namespace PROBD1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngredientController : ControllerBase
    {
        private masterContext _context;
        public IngredientController(masterContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult getIngredients()
        {
            return Ok(_context.Ingredient.ToList());
        }

        [HttpGet("{id:int}")]
        public IActionResult getIngredient(int id)
        {
            var ingred = _context.Ingredient.FirstOrDefault(e => e.Id == id);
            if (ingred == null)
            {
                return NotFound();
            }

            return Ok(ingred);
        }

        [HttpPost]
        public IActionResult postIngredient([FromBody]Ingredient ingredient)
        {
            _context.Ingredient.Add(ingredient);
            _context.SaveChanges();

            return Accepted();
        }

        [HttpPut("{id:int}")]
        public IActionResult putIngredient(int id, [FromBody]Ingredient Ingredient)
        {
            var ingr =  _context.Ingredient.FirstOrDefault(e => e.Id == id);
            if (ingr == null)
            {
                return NotFound();
            }
            else
            {

                ingr.Name = Ingredient.Name;
                _context.SaveChanges();
            }

            return Ok(ingr);
        }



    }
}