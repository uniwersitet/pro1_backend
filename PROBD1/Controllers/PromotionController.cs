﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROBD1.Models;

namespace PROBD1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionController : ControllerBase
    {
        private masterContext _context;
        public PromotionController(masterContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult getPromotions()
        {
            return Ok(_context.Promotion.ToList());
        }

        [HttpGet("{id:int}")]
        public IActionResult getPromotion(int id)
        {
            var promotion = _context.Promotion.FirstOrDefault(e => e.Id == id);

            if (promotion == null)
            {
                return NotFound();
            }

            return Ok(promotion);
        }

        [HttpPost]
        public IActionResult postPromotion([FromBody]Promotion promo)
        {
            _context.Promotion.Add(promo);
            _context.SaveChanges();

            return Accepted();
        }

        [HttpPut("{id:int}")]
        public IActionResult putPromotion(int id, [FromBody]Promotion promo)
        {
            var promotion = _context.Promotion.FirstOrDefault(e => e.Id == id);
            if (promotion == null)
            {
                return NotFound();
            }
            else
            {
                promotion.PizzaId = promo.PizzaId;
                promotion.DueAmount = promo.DueAmount;
            }

            return Ok(promotion);
        }

    }
}