﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PROBD1.Models;

namespace PROBD1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KoszykController : ControllerBase
    {
        private readonly masterContext _context;

        public KoszykController(masterContext context)
        {
            _context = context;
        }

        // GET: api/Koszyk
        [HttpGet]
        public IActionResult GetKoszyk()
        {
            return Ok(_context.Koszyk.ToList());
        }

        // GET: api/Koszyk/5
        [HttpGet("{id:int}")]
        public IActionResult GetKoszyk( int id)
        {
            var koszyk = _context.Koszyk.FirstOrDefault(e => e.Id == id);

            if (koszyk == null)
            {
                return NotFound();
            }

            return Ok(koszyk);
        }

        // PUT: api/Koszyk/5
        [HttpPut("{id:int}")]
        public IActionResult PutKoszyk(int id, [FromBody] Koszyk koszyk)
        {
            var kosz = _context.Koszyk.FirstOrDefault(e => e.Id == id);
            if (kosz == null)
            {
                return NotFound();
            }
            else
            {
                kosz.PizzaId = koszyk.PizzaId;
                kosz.PromotionId = koszyk.PromotionId;
                kosz.ClientId = koszyk.ClientId;
                _context.SaveChanges();
            }

            return Ok(kosz);
        }

        // POST: api/Koszyk
        [HttpPost]
        public IActionResult PostKoszyk([FromBody] Koszyk koszyk)
        {
            _context.Koszyk.Add(koszyk);
            _context.SaveChanges();
            
            return Accepted();
        }

        // DELETE: api/Koszyk/5
        [HttpDelete("{id:int}")]
        public IActionResult DeleteKoszyk(int id)
        {
            var koszyk = _context.Koszyk.FirstOrDefault(e => e.Id == id);
            _context.Koszyk.Remove(koszyk);
            _context.SaveChanges();

            return Ok(_context.Koszyk.ToList());
        }
    }
}