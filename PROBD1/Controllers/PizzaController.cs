﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PROBD1.Models;

namespace PROBD1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PizzaController : ControllerBase
    {
        private masterContext _context;
        public PizzaController(masterContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult getPizzas()
        {
            return Ok(_context.Pizza.ToList());
        }

        [HttpGet("{id:int}")]
        public IActionResult getPizza(int id)
        {
            var pizza = _context.Pizza.FirstOrDefault(e => e.Id == id);

            if (pizza == null){
                return NotFound();
            }

            return Ok(pizza);
        }

        [HttpPost]
        public IActionResult postPizza([FromBody]Pizza pizza)
        {
            _context.Pizza.Add(pizza);
            _context.SaveChanges();

            return Accepted();
        }

        [HttpPut("{id:int}")]
        public IActionResult putIngredient(int id, [FromBody]Pizza pizza)
        {
            var zza = _context.Pizza.FirstOrDefault(e => e.Id == id);
            if (zza == null)
            {
                return NotFound();
            }
            else
            {

                zza.Name = pizza.Name;
                _context.SaveChanges();
            }

            return Ok(zza);
        }

    }
}